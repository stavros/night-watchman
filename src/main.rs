#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate notify;
extern crate simple_logger;

use clap::{App, Arg};
use log::LevelFilter;
use notify::{RecommendedWatcher, RecursiveMode, Watcher};
use simple_logger::SimpleLogger;
use std::env;
use std::process::{Command, Stdio};
use std::sync::mpsc::channel;
use std::sync::Mutex;
use std::thread;
use std::time::{Duration, Instant};

lazy_static! {
    static ref LAST_EVENT_TIME: Mutex<Option<Instant>> = Mutex::new(None);
}

fn run_command(command: &Vec<String>, clear: bool) {
    if clear {
        Command::new("/usr/bin/clear").spawn().unwrap();
    }

    let mut cmd = Command::new(command[0].to_string())
        .args(command[1..].into_iter())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn()
        .unwrap();

    cmd.wait().unwrap();
}

/// Receive a filesystem event and act on it.
///
/// This does the proper debouncing (on top of the inotify debouncer) to emit the necessary
/// messages to the console when we are expecting changes/are done with changes.
fn debounce_events(fs_event: bool, timeout: u64, command: &Vec<String>, clear: bool) {
    // If this is a filesystem event, just reset the timer.
    let mut last_event_time = LAST_EVENT_TIME.lock().unwrap();

    if fs_event {
        if last_event_time.is_none() {
            // This is the first event for now, so emit the log line.
            info!("Change detected in filesystem.");
        }

        // Reset the timestamp to now.
        *last_event_time = Some(Instant::now());
        return;
    } else {
        // This is a timer event, so we should check when the last timer event was.
        if last_event_time.is_none()
            || Instant::now()
                .duration_since(last_event_time.unwrap())
                .as_secs()
                < timeout
        {
            // It there was no event, or there hasn't been long enough since the last one, return.
            return;
        }
        info!("Running command...");
        run_command(command, clear);

        // We added files to IPFS, so we'll set the last_event_time to None.
        *last_event_time = None;
    }
}

/// Run debounce_events() every second, for debouncing.
///
/// debounce_event() stores the last time an event occurred, and only runs a command if there's no
/// event after a while. To do that, however, it needs to also run "after a while". This is where
/// this function comes in. It runs debounce_events every second, to give it the opportunity to check
/// its timer and run the command if it needs to.
fn cron_thread(timeout: u64, command: Vec<String>, clear: bool) {
    loop {
        thread::sleep(Duration::from_secs(1));
        debounce_events(false, timeout, &command, clear);
    }
}

/// Handle the inotify events in the Hearth directory.
fn files_changed(op: notify::Op, path: std::path::PathBuf) {
    debug!("Got new event: {:?}, {:?}", op, path);
    if op.intersects(
        notify::op::CREATE
            | notify::op::REMOVE
            | notify::op::RENAME
            | notify::op::WRITE
            | notify::op::RESCAN
            | notify::op::CLOSE_WRITE,
    ) {
        debounce_events(true, 0, &Vec::new(), false);
    }
}

fn watch() {
    let (tx, rx) = channel();
    let mut watcher: RecommendedWatcher = Watcher::new_raw(tx).unwrap();
    let current_dir = env::current_dir().expect("Failed to determine current directory.");

    watcher
        .watch(current_dir, RecursiveMode::Recursive)
        .expect("There was an error when trying to watch the current directory for changes.");

    loop {
        match rx.recv() {
            Ok(notify::RawEvent {
                path: Some(path),
                op: Ok(op),
                cookie: _cookie,
            }) => files_changed(op, path),
            Ok(event) => debug!("Broken event: {:?}", event),
            Err(event) => debug!("Watch error: {:?}", event),
        }
    }
}

fn main() {
    let matches = App::new("night-watchman")
        .version("0.1")
        .author("Stavros Korokithakis")
        .about("Watch a directory for changes and run a command when it settles.")
        .arg(
            Arg::new("clear")
                .short('c')
                .long("clear")
                .about("Clear the shell before running the command"),
        )
        .arg(
            Arg::new("debug")
                .short('d')
                .long("debug")
                .about("Print debugging information"),
        )
        .arg(
            Arg::new("timeout")
                .short('t')
                .long("timeout")
                .value_name("SECONDS")
                .about("Sets a timeout after which a directory is considered settled")
                .takes_value(true)
                .default_value("10"),
        )
        .arg(
            Arg::new("watch")
                .short('w')
                .long("watch")
                .value_name("DIR")
                .about("The directory to watch")
                .takes_value(true)
                .default_value("."),
        )
        .arg(
            Arg::new("command")
                .about("The command to run on changes")
                .required(true)
                .multiple(true),
        )
        .get_matches();

    SimpleLogger::new()
        .with_level(if matches.is_present("debug") {
            LevelFilter::Debug
        } else {
            LevelFilter::Warn
        })
        .init()
        .unwrap();

    let command = matches
        .values_of("command")
        .unwrap()
        .map(|s| s.to_string())
        .collect();

    let dir = &matches.value_of("watch").unwrap();
    debug!("Switching to {:?}...", dir);
    env::set_current_dir(dir).expect("Invalid directory specified.");

    let timeout: u64 = matches.value_of_t("timeout").unwrap();

    thread::spawn(move || cron_thread(timeout, command, matches.is_present("clear")));

    watch();
}
