Night Watchman
==============

Night Watchman is a small utility that listens for changes to files in
the current directory, waits for the changes to stop for a bit, then
runs a command you specify.


Installation
------------

For now, you have to download a binary from the [builds
page](https://gitlab.com/stavros/night-watchman/-/pipelines?page=1&scope=all&ref=master&status=success)
and unzip it somewhere accessible, like ~/.local/bin/. That's it.

Usage
-----

Using Night Watchman is pretty simple:

```
$ night-watchman scp * remote.server.com:~/
```

This will copy all the files in the current directory to your home
directory on remote.server.com when files are unchanged for more than
ten seconds.
